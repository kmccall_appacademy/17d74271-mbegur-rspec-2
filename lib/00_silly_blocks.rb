def reverser
  reversed_words = yield.split.map { |word| word.reverse }
  reversed_words.join(" ")
end

def adder(starting_value = 1)
  yield + starting_value
end

def repeater(number_of_times = 1)
  number_of_times.times { yield }
end
