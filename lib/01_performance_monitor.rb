def measure(number_of_times = 1)
start_time = Time.now
number_of_times.times { |x| yield x }
end_time = Time.now
total = (end_time - start_time)/number_of_times
end
